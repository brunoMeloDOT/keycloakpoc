package com.example.keycloakpoc

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import net.openid.appauth.*
import androidx.databinding.DataBindingUtil
import com.example.keycloakpoc.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var authState: AuthState
    private lateinit var authRequest: AuthorizationRequest
    private lateinit var authLabel: TextView
    private val serviceConfig by lazy {
        AuthorizationServiceConfiguration(
            Uri.parse("https://pronto-keycloak.hom.dotgroup.com.br/auth/realms/yakisobarealm/protocol/openid-connect/auth"),
            Uri.parse("https://pronto-keycloak.hom.dotgroup.com.br/auth/realms/yakisobarealm/protocol/openid-connect/token")
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewBinding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        authLabel = viewBinding.textViewAuthorizationCode
        viewBinding.btnAuthorize.setOnClickListener {
            doAuthorization()
        }
    }

    override fun onResume() {
        super.onResume()
        authState = AuthState(serviceConfig)

        authRequest = AuthorizationRequest.Builder(
            serviceConfig,  // the authorization service configuration
            "yakisobaclient",  // the client ID, typically pre-registered and static
            ResponseTypeValues.CODE,  // the response_type value: we want a code
            Uri.parse("keycloakpoc://redirect.receiver")
        ).build()
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_AUTH) {
            val resp = AuthorizationResponse.fromIntent(data!!)
            val ex = AuthorizationException.fromIntent(data)

            if(ex == null) {
                println("Keycloakpoc -> authorizationCode=${resp?.authorizationCode}")
                authLabel.text = "Authorization Code = ${resp?.authorizationCode}"
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        println("BTEST -> onNewIntent")
    }

    private fun doAuthorization() {
        val authService = AuthorizationService(this)
        val authIntent = authService.getAuthorizationRequestIntent(authRequest)
        startActivityForResult(authIntent, RC_AUTH)
    }

    companion object {
        const val RC_AUTH = 999
    }
}